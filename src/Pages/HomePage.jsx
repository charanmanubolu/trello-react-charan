import React, { useEffect, useState } from 'react';
import getBoards from '../apis/boards/getBoards';
import AddBoard from '../components/AddBoard';
import { Box, Container, Typography, Alert } from '@mui/material';
import { NavLink } from 'react-router-dom';

function HomePage() {
  const [boards, setBoards] = useState([])
  const [error, setError] = useState("")

  const fetchBoards = async () => {
    try {
      const data = await getBoards()
      setBoards(data)
    } catch (error) {
      console.error("An error occurred while fetching the boards:", error)
      setError("Failed to fetch boards. Please try again later.")
    }
  };

  useEffect(() => {
    fetchBoards()
  }, []);

  return (
    <Container sx={{ display: 'flex', flexWrap: 'wrap' }}>
      <AddBoard setBoards={setBoards}/>
      {error && <Alert severity="error" sx={{ width: '100%' }}>{error}</Alert>}
      {
        
          boards.map((item) => (
            <NavLink to={`/boards/${item.id}`} key={item.id}>
              <Box sx={{
                width: 200,
                height: 100,
                margin: 3,
                backgroundColor: 'primary.main',
                '&:hover': {
                  backgroundColor: 'primary.dark',
                },
                padding: 2,
                color: 'white',
              }}>
                <Typography>{item.name}</Typography>
              </Box>
            </NavLink>
          ))
       
      }
    </Container>
  );
}

export default HomePage;
