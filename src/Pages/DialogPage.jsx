import React, { useState, useEffect, useRef } from "react";
import {
  Box,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Typography,
  Alert,
} from "@mui/material";
import getCheckList from "../apis/checkList/getCheckList";
import InputField from "../components/InputField.jsx";
import CheckList from "../components/CheckList.jsx";
import createCheckList from "../apis/checkList/createCheckList.js";
import getCheckItems from "../apis/checkItem/getCheckItems.js";

export default function ScrollDialog({ name, cardID }) {
  const [open, setOpen] = useState(false);
  const [scroll, setScroll] = useState("paper");
  const [checkList, setCheckList] = useState([]);
  const [checkItems, setCheckItems] = useState({});
  const [error, setError] = useState("");

  const descriptionElementRef = useRef(null);

  useEffect(() => {
    if (open) {
      fetchCheckList(cardID);
      const { current: descriptionElement } = descriptionElementRef;
      if (descriptionElement !== null) {
        descriptionElement.focus();
      }
    }
  }, [open, cardID]);

  const handleClickOpen = (scrollType) => () => {
    setOpen(true);
    setScroll(scrollType);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const fetchCheckList = async (cardID) => {
    try {
      const data = await getCheckList(cardID);
      setCheckList(data)

      data.forEach(async (checklist) => {
        try {
          const items = await getCheckItems(checklist.id);
          setCheckItems((prevItems) => ({
            ...prevItems,
            [checklist.id]: items
          }))
        } catch (error) {
          console.error("An error occurred while fetching check items:", error)
          setError("Failed to fetch check items. Please try again later.")
        }
      });
    } catch (error) {
      console.error("An error occurred while fetching the checklist:", error)
      setError("Failed to fetch checklist. Please try again later.")
    }
  };

  const handleAddCheckList = async (cardID, name) => {
    try {
      const newCheckList = await createCheckList(cardID, name);
      setCheckList((prevCheckList) => [...prevCheckList, newCheckList])
    } catch (error) {
      console.error("An error occurred while adding a new checklist:", error)
      setError("Failed to add new checklist. Please try again.")
    }
  };

  return (
    <>
      <Box
        onClick={handleClickOpen("body")}
        sx={{
          color: "black",
          width: "200px",
          display: "flex",
          justifyContent: "start",
        }}
      >
        <Typography>{name}</Typography>
      </Box>
      <Dialog
        open={open}
        onClose={handleClose}
        scroll={scroll}
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle
          id="scroll-dialog-title"
          sx={{ padding: 2, fontSize: 25, fontWeight: 700 }}
        >
          {name}
        </DialogTitle>
        <Box sx={{ margin: 1 }}>
          <InputField
            name="CheckList"
            onSubmit={(cardName) => handleAddCheckList(cardID, cardName)}
          />
        </Box>
        <DialogContent dividers={scroll === "paper"}>
          {error && <Alert severity="error">{error}</Alert>}
          <DialogContentText
            id="scroll-dialog-description"
            ref={descriptionElementRef}
            tabIndex={-1}
            sx={{ width: "500px" }}
          >
            {checkList.map((list) => (
              <CheckList
                key={list.id}
                list={list}
                checkItems={checkItems[list.id]}
                setCheckItems={setCheckItems}
                setCheckList={setCheckList}
              />
            ))}
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </>
  );
}
