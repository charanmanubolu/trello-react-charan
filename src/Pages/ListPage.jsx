import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import InputField from "../components/InputField";
import { Container, Typography, Box, Alert } from "@mui/material";
import ClearTwoToneIcon from "@mui/icons-material/ClearTwoTone";
import getList from "../apis/lists/getList";
import archiveList from "../apis/lists/archiveList";
import createNewList from "../apis/lists/createNewList";
import Cards from "../components/CardsContainer";

function ListPage() {
  const { id } = useParams()
  const [list, setList] = useState([])
  const [error, setError] = useState("")

  const fetchList = async () => {
    try {
      const listArray = await getList(id)
      setList(listArray)
    } catch (error) {
      console.error("An error occurred while fetching the list:", error)
      setError("Failed to fetch list. Please try again later.")
    }
  };

  useEffect(() => {
    fetchList();
  }, []);

  const handleAddList = async (name) => {
    try {
      let newList = await createNewList(name, id)
      setList((prevList) => [...prevList, newList])
    } catch (error) {
      console.error("An error occurred while adding a new list:", error)
      setError("Failed to add new list. Please try again.")
    }
  };

  const handleArchiveList = async (listId) => {
    try {
      await archiveList(listId)
      setList((prevList) => prevList.filter((list) => list.id !== listId))
    } catch (error) {
      console.error("An error occurred while archiving the list:", error)
      setError("Failed to archive list. Please try again.")
    }
  };

  return (
    <Container sx={{ display: "flex", width: "100vw", height: "80vh" }}>
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          borderRadius: 4,
          overflowX: "auto",
          width: "100%",
        }}
      >
        {error && <Alert severity="error" sx={{ width: "100%" }}>{error}</Alert>}
        {list.map((item) => (
          <Box
            key={item.id}
            sx={{
              minWidth: "300px",
              height: "fit-content",
              margin: 2,
              padding: 2,
              boxShadow: 5,
              borderRadius: 4,
            }}
          >
            <Typography
              sx={{
                marginLeft: 1,
                fontSize: 25,
                fontWeight: 700,
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              {item.name}
              <ClearTwoToneIcon
                sx={{ marginTop: 1, cursor: 'pointer' }}
                onClick={() => handleArchiveList(item.id)}
              />
            </Typography>
            <Cards listId={item.id} />
          </Box>
        ))}
        <Box
          sx={{
            paddingX: 2,
            margin: 2,
            boxShadow: 2,
            height: "fit-content",
            borderRadius: 4,
            display: "inline-block",
            minWidth: "300px",
          }}
        >
          <InputField name="another list" onSubmit={(name) => handleAddList(name)} />
        </Box>
      </Box>
    </Container>
  );
}

export default ListPage;
