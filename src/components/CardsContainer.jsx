import React, { useEffect, useState } from "react";
import { Box, Alert } from "@mui/material";
import DeleteRoundedIcon from "@mui/icons-material/DeleteRounded";
import getCardsInAList from "../apis/cards/getCards";
import createNewCard from "../apis/cards/createNewCard";
import archiveCard from "../apis/cards/archiveCard";
import ScrollDialog from "../Pages/DialogPage";
import InputField from "./InputField";

function Cards({ listId }) {
  const [cards, setCards] = useState([]);
  const [error, setError] = useState("");

  const fetchCards = async () => {
    try {
      const cardsInList = await getCardsInAList(listId);
      setCards(cardsInList);
    } catch (error) {
      console.error("An error occurred while fetching cards:", error);
      setError("Failed to fetch cards. Please try again later.");
    }
  };

  useEffect(() => {
    fetchCards();
  }, [listId]);

  const handleAddCard = async (cardName) => {
    try {
      let newCard = await createNewCard(cardName, listId);
      setCards((prevCards) => [...prevCards, newCard])
    } catch (error) {
      console.error("An error occurred while adding a new card:", error)
      setError("Failed to add new card. Please try again.")
    }
  };

  const handleArchiveCard = async (cardId) => {
    try {
      await archiveCard(cardId)
      setCards((prevCards) => prevCards.filter((card) => card.id !== cardId));
    } catch (error) {
      console.error("An error occurred while archiving the card:", error);
      setError("Failed to archive card. Please try again.");
    }
  };

  return (
    <>
      {error && <Alert severity="error">{error}</Alert>}
      {cards.map((card) => (
        <Box
          key={card.id}
          sx={{
            padding: "10px",
            backgroundColor: "lightgray",
            margin: 1,
            display: "flex",
            justifyContent: "space-between",
            fontSize: 20,
          }}
        >
          <ScrollDialog name={card.name} cardID={card.id} />
          <DeleteRoundedIcon onClick={() => handleArchiveCard(card.id)} />
        </Box>
      ))}
      <InputField
        name="card"
        onSubmit={(cardName) => handleAddCard(cardName)}
      />
    </>
  );
}

export default Cards;
