import React,{useState} from "react";
import { Box, Button, Alert } from "@mui/material";
import InputField from "./InputField.jsx";
import CheckListItem from "./CheckListItem.jsx";
import deleteCheckList from "../apis/checkList/deleteCheckList";
import getCheckItems from "../apis/checkItem/getCheckItems";
import createCheckItems from "../apis/checkItem/createCheckItem";

const CheckList = ({ list, checkItems, setCheckItems, setCheckList }) => {
  const [error, setError] = useState("")

  const handleDeleteCheckList = async (cardID, checkListID) => {
    try {
      await deleteCheckList(cardID, checkListID)
      setCheckList((prevCheckList) =>
        prevCheckList.filter((item) => item.id !== checkListID)
      )
    } catch (err) {
      console.error("An error occurred while deleting the checklist:", err)
      setError("Failed to delete checklist. Please try again.")
    }
  };

  const handleAddCheckItems = async (name, checkListID) => {
    try {
      await createCheckItems(name, checkListID)
      const items = await getCheckItems(checkListID)
      setCheckItems((prevItems) => ({
        ...prevItems,
        [checkListID]: items,
      }))
    } catch (err) {
      console.error("An error occurred while adding check items:", err)
      setError("Failed to add check item. Please try again.")
    }
  };

  return (
    <React.Fragment>
      {error && <Alert severity="error">{error}</Alert>}
      <Box
        sx={{
          padding: 1,
          fontSize: 25,
          fontWeight: 700,
          bgcolor: "lightgray",
          marginBottom: 2,
        }}
      >
        {list.name}
        <Button
          sx={{ float: "right" }}
          onClick={() => handleDeleteCheckList(list.idCard, list.id)}
        >
          Delete
        </Button>
      </Box>
      {checkItems &&
        checkItems.map((checkItem) => (
          <CheckListItem
            cardID={list.idCard}
            key={checkItem.id}
            checkItem={checkItem}
            checkListID={list.id}
            setCheckItems={setCheckItems}
          />
        ))}
      <Box sx={{ margin: 1 }}>
        <InputField name="CheckItem" onSubmit={(name) => handleAddCheckItems(name, list.id)} />
      </Box>
    </React.Fragment>
  );
};

export default CheckList;
