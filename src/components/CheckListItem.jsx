import React, { useState } from "react";
import { Box, Button, Checkbox, Alert } from "@mui/material";
import deleteCheckItem from "../apis/checkItem/deleteCheckItem";
import updateCheckItems from "../apis/checkItem/updateCheckItems";

const CheckListItem = ({ cardID, checkItem, checkListID, setCheckItems }) => {
  const [error, setError] = useState("")

  const handleDeleteCheckItem = async (checkItemID) => {
    try {
      await deleteCheckItem(checkListID, checkItemID)
      setCheckItems((prevItems) => ({
        ...prevItems,
        [checkListID]: prevItems[checkListID].filter(
          (item) => item.id !== checkItemID
        )
      }))
    } catch (err) {
      console.error("An error occurred while deleting the check item:", err)
      setError("Failed to delete check item. Please try again.")
    }
  }

  const handleUpdateCheckItems = async () => {
    const status = checkItem.state === "incomplete" ? "complete" : "incomplete"
    try {
      await updateCheckItems(cardID, checkItem.id, checkListID, status)
      setCheckItems((prevItems) => ({
        ...prevItems,
        [checkListID]: prevItems[checkListID].map((item) =>
          item.id === checkItem.id ? { ...item, state: status } : item
        )
      }))
    } catch (err) {
      console.error("An error occurred while updating the check item:", err)
      setError("Failed to update check item. Please try again.")
    }
  }

  return (
    <Box sx={{ margin: 1, paddingLeft: 1, display: "flex", justifyContent: "space-between" }}>
      {error && <Alert severity="error">{error}</Alert>}
      <Box onClick={handleUpdateCheckItems} sx={{ display: "flex", alignItems: "center" }}>
        <Checkbox checked={checkItem.state !== "incomplete"} />
        {checkItem.name}
      </Box>
      <Button onClick={() => handleDeleteCheckItem(checkItem.id)}>
        Delete
      </Button>
    </Box>
  )
}

export default CheckListItem
