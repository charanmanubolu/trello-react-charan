import React, { useState } from 'react';
import { Box, Button, TextField, Typography } from '@mui/material';

export default function InputField({ name, onSubmit }) {
  const [isVisible, setIsVisible] = useState(false);
  const [inputValue, setInputValue] = useState('');

  const handleInputVisibility = () => {
    setIsVisible(true);
  };

  const handleBlur = () => {
    setIsVisible(false);
    setInputValue('');
  };

  const handleSubmit = (event) => {
    event.preventDefault()
    onSubmit(inputValue)
     handleBlur();
  };

  return (
    <>
      {isVisible ? (
        <Box
          component="form"
          sx={{
            marginBottom: 3,
            borderRadius: 10,
          }}
          onSubmit={handleSubmit}
          //  onBlur={handleBlur}
        >
          <TextField
            autoFocus
            variant="standard"
            value={inputValue}
            onChange={(e) => setInputValue(e.target.value)}
            placeholder={`Enter ${name}`}
            InputProps={{ disableUnderline: true }}
            sx={{
              height: '40px',
              padding: 1,
              background: 'white',
              outline: 'none',
              width: '100%',
            }}
          />
          <Button type="submit" variant="contained">
            Add {name}
          </Button>
          <Button
            onClick={handleBlur}
            sx={{ fontSize: '30px', fontWeight: 900, color: 'red' }}
          >
            X
          </Button>
        </Box>
      ) : (
        <Box>
          <Typography
            onClick={handleInputVisibility}
            sx={{ padding: 1, margin: 1 }}
          >
            + Add {name}
          </Typography>
        </Box>
      )}
    </>
  );
}
