import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import Alert from "@mui/material/Alert";
import { useState } from "react";
import createBoard from "../apis/boards/createBoard";

export default function AddBoard({ setBoards }) {
  const [open, setOpen] = useState(false)
  const [error, setError] = useState("")

  const handleClickOpen = () => {
    setOpen(true)
  };

  const handleClose = () => {
    setOpen(false)
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const formData = new FormData(event.currentTarget)
    const formJson = Object.fromEntries(formData.entries())
    let name = formJson.name

    try {
      const newBoard = await createBoard(name)
      setBoards((prev) => [...prev, newBoard])
      handleClose()
    } catch (error) {
      console.error("An error occurred while creating the board:", error)
      setError("Failed to create board. Please try again.")
    }
  };

  return (
    <>
      <Button
        variant="outlined"
        onClick={handleClickOpen}
        sx={{
          margin: 3,
          color: "white",
          width: 230,
          height: 130,
          padding: 2,
          backgroundColor: "#9e9e9e",
          "&:hover": {
            backgroundColor: "#616161",
          },
        }}
      >
        Create New Board
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        PaperProps={{
          component: "form",
          onSubmit: handleSubmit,
        }}
      >
        <DialogTitle>Create New Board</DialogTitle>
        <DialogContent>
          {error && <Alert severity="error">{error}</Alert>}
          <TextField
            autoFocus
            required
            margin="dense"
            id="name"
            name="name"
            label="Board Name"
            type="text"
            fullWidth
            variant="standard"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button type="submit">Create</Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
