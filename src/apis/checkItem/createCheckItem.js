import axios from "axios";
import { APIKey, APIToken } from "../../utils/ApiKey";

const createCheckItems = async (checkListName, checklistId) => {
  try {
    const response = await axios.post(`
    https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${APIKey}&token=${APIToken}&name=${checkListName}`);
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export default createCheckItems;
