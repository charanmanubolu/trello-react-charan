import axios from 'axios';
import {APIKey,APIToken} from '../../utils/ApiKey';

const deleteCheckItem= async (listId,idCheckItem)=>{
  try{
    const response = await axios.delete(`https://api.trello.com/1/checklists/${listId}/checkItems/${idCheckItem}?key=${APIKey}&token=${APIToken}`);
    return response
  }catch(err){
    console.error(err);
  }
    
}

export default deleteCheckItem