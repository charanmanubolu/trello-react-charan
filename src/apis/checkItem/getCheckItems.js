import axios from "axios";
import { APIKey, APIToken } from "../../utils/ApiKey";

const getCheckItems = async (checklistId) => {
  try {
    const response = await axios.get(
      `https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${APIKey}&token=${APIToken}`
    );
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export default getCheckItems;
