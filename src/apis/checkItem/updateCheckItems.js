import axios from "axios";
import { APIKey, APIToken } from "../../utils/ApiKey";

const updateCheckItems = async (cardId, checkItemId, checkListId, status) => {
  try {
    const response = await axios.put(`
        https://api.trello.com/1/cards/${cardId}/checklist/${checkListId}/checkItem/${checkItemId}/state?key=${APIKey}&token=${APIToken}&value=${status} 
    `);
    return response.data;
  } catch (err) {
    console.log(err);
  }
};

export default updateCheckItems;
