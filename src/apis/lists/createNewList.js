import axios from "axios";
import { APIKey, APIToken } from "../../utils/ApiKey";

const createNewList = async (name, idBoard) => {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/lists?name=${name}&idBoard=${idBoard}&key=${APIKey}&token=${APIToken}`
    );
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export default createNewList;
