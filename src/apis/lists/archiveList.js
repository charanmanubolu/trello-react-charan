import axios from "axios";
import { APIKey, APIToken } from "../../utils/ApiKey";

const archiveList = async (id) => {
  try {
    const response = await axios.put(
      `https://api.trello.com/1/lists/${id}/closed?value=true&key=${APIKey}&token=${APIToken}`
    );
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export default archiveList;
