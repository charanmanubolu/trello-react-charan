import axios from "axios";
import { APIKey, APIToken } from "../../utils/ApiKey";

const createNewCard = async (name, id) => {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/cards?idList=${id}&name=${name}&key=${APIKey}&token=${APIToken}`
    );
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export default createNewCard;
