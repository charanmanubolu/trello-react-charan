import axios from "axios";
import { APIKey, APIToken } from "../../utils/ApiKey";

const getCardsInAList = async (id) => {
  try {
    const response = await axios.get(
      `https://api.trello.com/1/lists/${id}/cards?key=${APIKey}&token=${APIToken}`
    );
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export default getCardsInAList;
