import axios from "axios";
import { APIKey, APIToken } from "../../utils/ApiKey";

const archiveCard = async (id) => {
  try {
    const response = await axios.delete(
      `https://api.trello.com/1/cards/${id}?key=${APIKey}&token=${APIToken}`
    );
    return response.data;
  } catch (err) {
    console.error(err);
  }
};

export default archiveCard;
