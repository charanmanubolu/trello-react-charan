import axios from "axios";
import { APIKey, APIToken } from "../../utils/ApiKey";

const createCheckList = async (cardId, checkListName) => {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/cards/${cardId}/checklists?name=${checkListName}&key=${APIKey}&token=${APIToken}`
    );
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export default createCheckList;
