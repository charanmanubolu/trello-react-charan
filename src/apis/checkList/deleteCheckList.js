import axios from "axios";
import { APIKey, APIToken } from "../../utils/ApiKey";

const deleteCheckList = async (id, idChecklist) => {
  try {
    const response = await axios.delete(
      `https://api.trello.com/1/cards/${id}/checklists/${idChecklist}?key=${APIKey}&token=${APIToken}`
    );
    return response.data;
  } catch (err) {
    console.log(err);
  }
};

export default deleteCheckList;
