import axios from 'axios';
import {APIKey,APIToken} from '../../utils/ApiKey';

const getCheckList = async (id)=>{
     try {
        const response = await axios.get(`https://api.trello.com/1/cards/${id}/checklists?key=${APIKey}&token=${APIToken}`);
        return response.data
     } catch (error) {
        console.log(error)
     }
  
    
}

export default getCheckList 