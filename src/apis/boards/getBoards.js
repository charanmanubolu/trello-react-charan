import axios from "axios";
import { APIKey, APIToken } from "../../utils/ApiKey";

const getBoards = async () => {
  try {
    const response = await axios.get(
      `https://api.trello.com/1/members/me/boards?key=${APIKey}&token=${APIToken}`
    );
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export default getBoards;
