import axios from "axios";
import { APIKey, APIToken } from "../../utils/ApiKey";

const createBoard = async (name) => {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/boards/?name=${name}&key=${APIKey}&token=${APIToken}`
    );
    return response.data;
  } catch (err) {
    console.log(err);
  }
};

export default createBoard;
