import './App.css'
import  ResponsiveAppBar  from './components/AppBar'
import { RouterProvider, createBrowserRouter } from 'react-router-dom'
import HomePage from './Pages/HomePage'
import ListPage from './Pages/ListPage'

function App() {
  

  const router = createBrowserRouter([
    {
      path: '/',
      element: <HomePage/>
    },{
      path: '/boards',
      element: <HomePage/>
    },
    {
      path: '/boards/:id',
      element: <ListPage/>
    }
    
  ])

  return (
    <>
    <ResponsiveAppBar/>
    <RouterProvider router={router}/>
    </>
  )
}

export default App
